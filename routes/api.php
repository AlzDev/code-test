<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Pet routes
Route::post('pet/{id}/uploadImage', 'PetsController@uploadImage');
Route::post('pet', 'PetsController@create');
Route::put('pet', 'PetsController@put');
Route::get('pet/findByStatus', 'PetsController@findByStatus');
Route::get('pet/{id}', 'PetsController@read');
Route::post('pet/{id}', 'PetsController@update');
Route::delete('pet/{id}', 'PetsController@delete');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
