<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use Illuminate\Support\Str;

use App\Pet;
use App\Photo;
use App\Category;
use App\TagJoin;
use App\CategoryJoin;

class PetsController extends Controller
{
    public function create(Request $request) {
        
        // Check the normal inputs of the pet first
        if(!$request->name || !$request->status){
            return response()->json(['code' => static::STATUS_INVALID_INPUT, 'message' => 'Please check the name and status of your pet!']);
        }
        
        // Check the category of the pet - would most likely check the database here too
        if(!$request->category['id'] || !$request->category['name']){
            return response()->json(['code' => static::STATUS_INVALID_INPUT, 'message' => 'Please check your pet category!']);
        }

        // Create the pet and save the details
        $pet = new Pet();
        $pet->name = $request->name;
        $pet->status = $request->status;
        $pet->save();

        // Create the join to the category
        $cat_join = new CategoryJoin();
        $cat_join->category_id = $request->category['id'];
        $cat_join->pet_id = $pet->id;
        $cat_join->save();

        // Create the link to tags from the tag array
        // At this point I assume tags are optional
        if($request->tags){
            foreach ($request->tags as $tag){
                // Create a link between the tags and the pet
                $tag_join = new TagJoin();
                // Retrieved from above created record
                $tag_join->pet_id = $pet->id;
                // Retrieved from array of tag in requests
                $tag_join->tag_id = $tag['id'];
                $tag_join->save();
            }
        }  

        // Finally, add the photo urls to the pet
        // At this point I assume photos are optional
        if($request->photoUrls){
            // Here im assuming the image might not have to be stored, just the URL
            foreach ($request->photoUrls as $photo_url){
                    $photo = new Photo();
                    // Retrieved from above created record
                    $photo->pet_id = $pet->id;
                    // Retrieved from array of photo urls
                    $photo->photoUrl = $photo_url['photoUrl'];
                    $photo->save();
            }
        }

        return response()->json(['code' => static::STATUS_SUCCESS, 'message' => 'Your pet has been saved!'])
                ->header('Content-Type', 'application/json');

    }

    public function update($id, Request $request) {
        // Check that there is actually an ID in the request
        if(!$id) {
            return response()->json(['code' => static::STATUS_INVALID_INPUT, 'message' => 'Please provide pet ID.']);
        }
        
        // Check the normal inputs of the pet first
        if(!$request->name || !$request->status){
            return response()->json(['code' => static::STATUS_INVALID_INPUT, 'message' => 'Please check the name and status of your pet!']);
        }
        
        // Check the category of the pet - would most likely check the database here too
        if(!$request->category['id'] || !$request->category['name']){
            return response()->json(['code' => static::STATUS_INVALID_INPUT, 'message' => 'Please check your pet category!']);
        } else {

        }

        // Create the pet and save the details
        $pet = Pet::where('id', $id)->first();
        $pet->name = $request->name;
        $pet->status = $request->status;
        $pet->save();

        // Delete the previous Categories to refresh
        CategoryJoin::where('pet_id', $id)->where('category_id', $request->category['id'])->delete();

        // Create the join to the category
        $cat_join = new CategoryJoin();
        $cat_join->category_id = $request->category['id'];
        $cat_join->pet_id = $pet->id;
        $cat_join->save();

        // Delete all the tags to have the tags fresh
        TagJoin::where('pet_id', $request->id)->delete();

        // Create the link to tags from the tag array
        if($request->tags){
            foreach ($request->tags as $tag){
                // Create a link between the tags and the pet
                $tag_join = new TagJoin();
                // Retrieved from above created record
                $tag_join->pet_id = $pet->id;
                // Retrieved from array of tag in requests
                $tag_join->tag_id = $tag['id'];
                $tag_join->save();
            }
        }        

        // Finally, add the photo urls to the pet
        if($request->photoUrls){
            // Here im assuming the image might not have to be stored, just the URL
            foreach ($request->photoUrls as $photo_url){
                    // Check if a photo with this url already exists for this pet
                    $existing_photo = Photo::where('photoUrl', $photo_url['photoUrl'])->where('pet_id', $pet->id)->get();
                        if($existing_photo){
                        $photo = new Photo();
                        // Retrieved from above created record
                        $photo->pet_id = $pet->id;
                        // Retrieved from array of photo urls
                        $photo->photoUrl = $photo_url['photoUrl'];
                        $photo->save();
                    }
            }
        }

        return response()->json(['code' => static::STATUS_SUCCESS, 'message' => 'Your pet has been updated!'])
                ->header('Content-Type', 'application/json');

    }

    // Upload an image for a pet
    public function uploadImage($id, Request $request){

        // Search for the pet based on the id
        $pet = Pet::where('id', $id)->first();

        if(!$pet){
            return response()->json(['code' => static::STATUS_ERROR, 'message' => 'Can\'t find your pet!']);
        } else {

            try{

                // Make sure there is a file being sent //TODO: Validate that its an image type
                if(!$request->file){
                    return response()->json(['code' => static::STATUS_ERROR, 'message' => 'Please upload an image']);
                }

                // The image would be uploaded as a "file" via FormData() - "Content-Type": "multipart/form-data" as the header
                $file = $request->file;

                // Create a unique id for the image
                $uuid = Str::uuid();

                // Save the file path and assign as a URL for later using the .env file
                $file_path = public_path('storage/avatars') . $id .'-' . $uuid . '.jpg';
                $url = env('APP_URL') . '/' . $file_path;

                // Store the file locally
                $request->file->move($file_path);

                // Create a new photo record
                $photo = new photo();
                $photo->pet_id = $id;
                $photo->photoUrl = $url;

                return response()->json(['code' => static::STATUS_SUCCESS, 'message' => $pet])
                ->header('Content-Type', 'application/json');

            } catch (Exception $e) {

                return response()->json(['code' => static::STATUS_ERROR, 'message' => $e]);
            }

        }
    }

    public function findByStatus(Request $request){
        $all_pets = [];
        foreach ($request->status as $status){
            $pets = Pet::where('status',$status)->get();
            array_push($all_pets, $pets);
        }
        return response()->json(['code' => static::STATUS_SUCCESS, 'message' => $all_pets])
                ->header('Content-Type', 'application/json');
    }

    public function read($id){
        $pets = Pet::where('id',$id)->get();
        
        return response()->json(['code' => static::STATUS_SUCCESS, 'message' => $pets])
                ->header('Content-Type', 'application/json');
    }

    public function delete($id){
        Pet::where('id',$id)->delete();
        
        return response()->json(['code' => static::STATUS_SUCCESS, 'message' => 'Pet successfully deleted.'])
                ->header('Content-Type', 'application/json');
    }
}
