<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    // Constants for code returned via API
    const STATUS_SUCCESS        = 200;
    const STATUS_ERROR          = 400;
    const STATUS_INVALID_INPUT  = 405;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
