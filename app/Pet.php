<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $table = 'pets';

    protected $fillable = [];

     // Get the Photos for this pet
     public function photos()
     {
         return $this->hasMany('App\Photo');
     }

     // Get the tags for this pet
     public function tags()
     {
         return $this->hasMany('App\TagJoin');
     }

     // Get the category for this pet
     public function category()
     {
         return $this->hasMany('App\TagJoin');
     }
}
